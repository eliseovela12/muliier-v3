$(document).ready(function () {
    let header = $('#header');
    /* Margin top automatico para main - dependiendo de la altura del header */
    let altura = $('#header').innerHeight();
    $("#main").css('margin-top', altura);
    
    $(window).resize(function(){
        $("#main").css('margin-top', altura);
    });


    const main_foro_info_product = $('.modify-product-info-margin');
    $(window).scroll(function(){
        if($(window).scrollTop() >= altura){
            main_foro_info_product.css('margin-top','300px');
        }else{
            $("#main").css('margin-top', altura);
        }
    });

    var width_window = $(window).width();


    /* Cambiando de fondo para footer - mobile o desktop */
    function windowResize(){
        const footer__body = $('.footer__body');

        if(width_window <= 860){
            $(footer__body).removeClass('back_footer_desktop');
            $(footer__body).addClass('back_footer_mobile');
        }else{
            $(footer__body).addClass('back_footer_desktop');
            $(footer__body).removeClass('back_footer_mobile');
        }
    }
    windowResize(),
    $(window).resize(windowResize);

    /* Ocultar parte de la cabecera al hacer scroll */
    const header_original = $('#header');
    const header_fixed = $('#header-fixed');
    
    if(width_window >= 860){
        $(window).scroll(function (e) { 
            if($(window).scrollTop() >= altura){
                $('#top_desktop').addClass('d-none');
                $('#bottom_desktop').addClass('d-none');
                $('#middle_desktop').addClass('box-shadow');
            }else{
                $('#top_desktop').removeClass('d-none');
                $('#bottom_desktop').removeClass('d-none');
                $('#middle_desktop').removeClass('box-shadow');
            }
        });
    }else{
        $('#top_desktop').removeClass('d-none');
        $('#bottom_desktop').removeClass('d-none');
        $('#middle_desktop').removeClass('box-shadow');
    }

    /* Mostrar buscador mobile */
    const container_search = $('#container-mobile-search');

    $('#toggle_search').click(function (e) { 
        e.preventDefault();
        container_search.toggleClass('d-none');
        $('#input_search_mobile').focus();
    });


    function mobile (){
        
        if($(window).width() <= 580){


            if($('.levels .level__item').length >= 3){
                $('.levels .level__item:last-child').addClass('d-none');
            }
        }

    }

    mobile();
    $(window).resize(mobile);
    
    $('#btn-write-comment').click(function(){
        abrirModal($('#modal-write-comment'));
    });

    $('#close-comment').click(function(){
        closeModal( $('#modal-write-comment'));
    });

    function closeModal(id_modal){
        id_modal.addClass('close-modal');
        id_modal.removeClass('show-modal');
    }

    function abrirModal(id_modal){
        id_modal.removeClass('close-modal');
        id_modal.addClass('show-modal');
    }
    

    /* Filtro de productos */
   /*  const btn_filtro_productos = $('#btn-cancle-filter-content');
    const contenedor_filtro_productos = $('#contenedor-filtro-producto');
    contenedor_filtro_productos.click(function(){
        contenedor_filtro_productos.addClass('d-none');
    }); */

    $('#open-container-filter').on('click',function(){
        console.log('Joder');
        $('#contenedor-filtro-producto').removeClass('d-none');
    });

    $('#btn-cancle-filter-content').on('click',function(){
        $('#contenedor-filtro-producto').addClass('d-none');
    });

    if($(window).width() < 1100){
        console.log('menor a 1100px');
        $('#contenedor-filtro-producto').addClass('d-none');
    }else{
        console.log('mayor a 1100px');
        $('#contenedor-filtro-producto').removeClass('d-none');
    }
   
    
    /* Height from hero image assesor */

    /* Open modal */

    /* Datos asesor registro */
    $('#open-registro-asesor').click(function(e){
        e.preventDefault();
        abrirModal($('#modal-register-asesor'));
    }); 
   
    $('#close-register-asesor').click(function(){
        closeModal( $('#modal-register-asesor'));
    });
   
    
    $('#open-login-asesor').click(function(e){
        e.preventDefault();
        abrirModal($('#modal-login-asesor'));
    }); 

    $('#close-login-asesor').click(function(){
        closeModal( $('#modal-login-asesor'));
    });

    /* abrirModal(e,modal_container,modal); */

    $('#open-login-profile').click(function(e){
        e.preventDefault();
        abrirModal($('#modal-login-main'));
    }); 

    $('#close-login-main').click(function(){
        closeModal( $('#modal-login-main'));
    });

    $('#open-register-options').click(function(e){
        e.preventDefault();
        abrirModal($('#modal-register-options'));
    }); 

    $('#close-register-options').click(function(){
        closeModal( $('#modal-register-options'));
    });
   

    /* Opciones de registro */

    $('#open-option-register-proveedor').click(function(e){
        closeModal( $('#modal-register-options'));
        e.preventDefault();
        abrirModal($('#modal-register-proveedor'));
    })

    $('#close-register-proveedor').click(function(){
        closeModal( $('#modal-register-proveedor'));
    });

    
    $('#open-option-register-asesor').click(function(e){
        closeModal( $('#modal-register-options'));
        e.preventDefault();
        abrirModal($('#modal-register-asesor'));
    })

    $('#close-register-asesor').click(function(){
        closeModal( $('#modal-register-asesor'));
    });


    $('#open-option-register-cliente').click(function(e){
        closeModal( $('#modal-register-options'));
        e.preventDefault();
        abrirModal($('#modal-register-client'));
    })

    $('#close-register-client').click(function(){
        closeModal( $('#modal-register-client'));
    });

    const content_butons_objectives = $('#content-objectives-button a');
    $('#content-objectives-button a').on('click',function(){
        const content_parent = $(this).parent();
        const link_active = content_parent.find('.active').removeClass('active');
        $(this).addClass('active');
    });

});

