
const firstSlidePage = document.querySelector('.page-iniciar-sesion');
const progressText = document.querySelectorAll('#progress-payment .step .text-step');
const progressCheck = document.querySelectorAll('#progress-payment .step .icono');
const progressNumber = document.querySelectorAll('#progress-payment .step .number');


/* Botones alternativos */
const login_next = document.querySelector('#chose-address');

const address_previous = document.querySelector('#return-session-page');
const adress_next = document.querySelector('#chose-payment');

const payment_previous = document.querySelector('#return-address-page');
const payment_next = document.querySelector('#chose-confirmation');

const confirmation_previous = document.querySelector('#return-payment-page');
const confirmation_next = document.querySelector('#chose-success');

const success_previous = document.querySelector('#return-confirmation-page');
/* const success_next = document.querySelector('#button-finaly');
 */

const firtNextAction = ()=>{
    firstSlidePage.style.marginLeft = '-20%';
};

let max = 5;
let current = 1;

const initialAction = ()=>{
    progressNumber[current - 1].classList.add('active');
    progressText[current - 1 ].classList.toggle('color-main');
};

initialAction();

login_next.addEventListener('click',function(){
    nextPage('-20%');
});

adress_next.addEventListener('click',function(){
    nextPage('-40%');
});

payment_next.addEventListener('click',function(){
    nextPage('-60%');
});

confirmation_next.addEventListener('click',function(){
    nextPage('-80%');
});


address_previous.addEventListener('click',function(){
    previous('0%');
});

payment_previous.addEventListener('click',function(){
    previous('-20%');
});

confirmation_previous.addEventListener('click',function(){
    previous('-40%');
});

/* success_previous.addEventListener('click',function(){
    previous('-60%');
}); */

function previous(percent){
    firstSlidePage.style.marginLeft = percent;
    progressCheck[current - 2].classList.remove('active');
    progressNumber[current - 1].classList.remove('active');
    progressText[current - 1].classList.remove('color-main');
    progressNumber[current - 2].classList.remove('d-none');
    progressCheck[current - 2].classList.toggle('d-none');
    current -= 1;
}

function nextPage(percent){
    firstSlidePage.style.marginLeft = percent;
    progressNumber[current - 1].classList.add('d-none');
    progressCheck[current - 1].classList.toggle('d-none');
    progressCheck[current - 1].classList.add('active');
    current += 1;
    progressNumber[current - 1].classList.add('active');
    progressText[current - 1].classList.toggle('color-main');
}

/* Selector para ctivar tipo de pago */

/* Radio select first */
const radio_active_payment = document.querySelectorAll('input[name="active-payment"]');

const box_content_card = document.querySelector('#box-open-card');
const box_content_delivery = document.querySelector('#box-open-delivery');

/* Radio select second */
const radio_card_type = document.querySelectorAll('input[name="card-type"]');
const radio_new_card = document.querySelector('#radio-new-card');
const radio_save_card = document.querySelector('#radio-save-card');

radio_active_payment.forEach((element,key) => {
    element.addEventListener('change', (event) => {
        console.log(key);
        if (event.currentTarget.checked && key == 0) {
            
            box_content_card.classList.remove('d-none');
            box_content_delivery.classList.add('d-none');
            
        } else {

            box_content_card.classList.add('d-none');
            box_content_delivery.classList.remove('d-none');
            
        }
    });
});


radio_card_type.forEach((element,key) => {
    element.addEventListener('change', (event) => {
        console.log(key);
        if (event.currentTarget.checked && key == 0) {
            
            radio_new_card.classList.remove('d-none');
            radio_save_card.classList.add('d-none');
            
        } else {

            radio_new_card.classList.add('d-none');
            radio_save_card.classList.remove('d-none');
            
        }
    });
});


