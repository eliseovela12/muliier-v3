$(document).ready(function(){
    jQuery.fn.exists = function(){return this.length>0;}
    
    if ($('#testimonials').exists()) {

        $('#testimonials').owlCarousel({
            dots:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    dots:true
                },
                600:{
                    items:2,
                    dots:true
                },
                1000:{
                    items:3,
                    dots:true
                }
            }
        });
    }


    if ($('#info-product').exists()) {

        $('#info-product').owlCarousel({
            dots:false,
            navText : ["",""],
            center:true,
            rewindNav : true,
            URLhashListener:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                }
            }
        });
    }
    

    if ($('#home-slider').exists()) {

        $('#home-slider').owlCarousel({
            responsiveClass:true,
            loop:true,
            autoplay:true,
            autoplayTimeout:3000,
            margin: 10,
            autoplayHoverPause:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                }
            }
        });
        

    }

    if ($('#slide-list-01').exists()) {
        $('#slide-list-01').owlCarousel({
            responsiveClass:true,
            dots:false,
            margin:24,
            slideBy:5,
            navText : ["",""],
            rewindNav : true,
            responsive: {
                0: {
                    items: 1,
                    nav:true
                },
                320: {
                    items: 1,
                    nav:true
                },
                600: {
                    items: 2,
                    nav:true
                },
                860:{
                    items:3,
                    nav:true
                },
                1100:{
                    items:4,
                    nav:true
                },
                1366:{
                    items: 5,
                    nav:true
                },
                1440:{
                    items:6,
                    nav:true
                }
            }
        });
    }

    if ($('#slide-list-02').exists()) {
        $('#slide-list-02').owlCarousel({
            /* center: true,
            items:3, */
            items:3,
            slideBy:5,
            margin:24,
            responsiveClass:true,
            dots:false,
            /* navText: ["<img src='../../img/home/arrow_left.svg'>","<img src='../../img/home/arrow_right.svg'>"], */
            navText : ["",""],
            rewindNav : true,
            responsive: {
                0: {
                    items: 1,
                    nav:true
                },
                320: {
                    items: 1,
                    nav:true
                },
                600: {
                    items: 2,
                    nav:true
                },
                860:{
                    items:3,
                    nav:true
                },
                1100:{
                    items:4,
                    nav:true
                },
                1366:{
                    items: 5,
                    nav:true
                },
                1440:{
                    items: 6,
                    nav:true
                }
            }
        });
    }

    if ($('#slide-list-03').exists()) {
        $('#slide-list-03').owlCarousel({
            responsiveClass:true,
            dots:false,
            margin:24,
            slideBy:5,
            navText : ["",""],
            rewindNav : true,
            responsive: {
                0: {
                    items: 1,
                    nav:true
                },
                320: {
                    items: 1,
                    nav:true
                },
                600: {
                    items: 2,
                    nav:true
                },
                860:{
                    items:3,
                    nav:true
                },
                1100:{
                    items:4,
                    nav:true
                },
                1366:{
                    items: 5,
                    nav:true
                },
                1440:{
                    items:6,
                    nav:true
                }
            }
        });
    }
    if ($('#history').exists()) {
        $('#history').owlCarousel({
            loop:true,
            nav:false,
            dots:true,
            items:1,
            autoplay:true,
            autoplaySpeed:500,
            smartSpeed:500,
            autoplayHoverPause:true,
        });
    }
    
    if ($('#slider-objectives').exists()) {
        $('#slider-objectives').owlCarousel({
            loop:false,
            nav:false,
            dots:false,
            items:1,
            URLhashListener:true
        });
    }


});
