"use strict";

window.onload = function () {
  /* Main top auto */
  var header = document.querySelector('#header');
  var main = document.querySelector('#main');
  var height_header = header.offsetHeight;
  main.style.marginTop = height_header + 'px';

  function windowResize() {
    var width_window = document.body.scrollWidth;
    var footer_body = document.querySelector('.footer__body');

    if (width_window <= 860) {
      footer_body.classList.remove('back_footer_desktop');
      footer_body.classList.add('back_footer_mobile');
    } else {
      footer_body.classList.remove('back_footer_mobile');
      footer_body.classList.add('back_footer_desktop');
    }
  }
  /* Background img auto (footer) */


  windowResize();
  window.addEventListener('resize', windowResize());
  /* Info productos */

  var contenedor_productos = document.querySelector('#product_container');
  var slide_item = document.querySelector('.owl-item .active');
  var owl_stage = document.querySelector('.slider__img--full .owl-stage-outer .owl-stage');
  console.log(owl_stage.childElementCount);
  console.log(owl_stage.indexOf('.owl-item .active'));
};