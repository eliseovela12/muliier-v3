"use strict";

$(document).ready(function () {
  $('#testimonials').owlCarousel({
    dots: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        dots: true
      },
      600: {
        items: 2,
        dots: true
      },
      1000: {
        items: 3,
        dots: true
      }
    }
  });
  $('#info-product').owlCarousel({
    dots: false,
    URLhashListener: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      }
    }
  });
});