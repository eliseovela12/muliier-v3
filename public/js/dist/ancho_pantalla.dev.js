"use strict";

(function () {
  window.addEventListener('resize', function () {
    var width = document.body.scrollWidth;
    var sidebar = document.querySelector('#sidebar');

    if (width >= 860) {
      sidebar.style.display = 'none';
    } else {
      sidebar.style.display = 'initial';
    }
  });
})();